{-# LANGUAGE BinaryLiterals #-}

module Zelinf.CryptoHW.FiniteField.GF2Spec where

import           Test.Hspec

import           Zelinf.CryptoHW.FiniteField.GF2.Internal (gf2MulInv)

spec :: Spec
spec = do
  describe "gf2MulInv" $ do
    it "returns 0b010 for rp = 0b1011 and x = 0b101" $ do
      gf2MulInv 0b1011 0b101 `shouldBe` 0b010
