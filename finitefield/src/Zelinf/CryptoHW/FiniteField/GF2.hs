{-# LANGUAGE DataKinds      #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE TypeOperators  #-}

module Zelinf.CryptoHW.FiniteField.GF2
  ( GF2
  , fromNatural
  , toNatural
  , getReducingPolynomial
  )
where

import           Data.Semigroup
import           GHC.TypeLits
import           Numeric.Natural

import           Zelinf.CryptoHW.FiniteField.Field
import           Zelinf.CryptoHW.FiniteField.GF2.Internal (gf2Modulo, gf2MulInv,
                                                           gf2Multiply)

newtype GF2 (rp :: Nat) = GF2 Natural
  deriving (Show, Eq, Ord)

toNatural :: GF2 rp -> Natural
toNatural (GF2 x) = x

fromNatural :: Natural -> GF2 rp
fromNatural = GF2

getReducingPolynomial :: KnownNat rp => GF2 rp -> Natural
getReducingPolynomial = fromIntegral . natVal

instance Semigroup (GF2 rp) where
  GF2 x <> GF2 y = GF2 (x ^ y)

instance Monoid (GF2 rp) where
  mempty = GF2 0
  mappend = (<>)

instance KnownNat rp => Field (GF2 rp) where
  one = GF2 1
  x'@(GF2 x) * GF2 y = GF2 $ gf2Modulo (gf2Multiply x y) (getReducingPolynomial x')
  mulInv (GF2 0)    = Nothing
  mulInv x'@(GF2 x) = Just $ GF2 $ gf2MulInv (getReducingPolynomial x') x
