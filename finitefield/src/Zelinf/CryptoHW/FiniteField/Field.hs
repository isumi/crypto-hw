module Zelinf.CryptoHW.FiniteField.Field
  ( Field(..)
  )
where

class Monoid a => Field a where
  one :: a
  (*) :: a -> a -> a
  mulInv :: a -> Maybe a
  {-# MINIMAL one , (*) , mulInv #-}
