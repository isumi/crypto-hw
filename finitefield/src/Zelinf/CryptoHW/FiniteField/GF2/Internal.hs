{-# LANGUAGE ScopedTypeVariables #-}

module Zelinf.CryptoHW.FiniteField.GF2.Internal where

import           Data.Bits
import           Data.Foldable                (foldl')
import           Math.NumberTheory.Logarithms (naturalLog2)
import           Numeric.Natural

{-| Multiplication in GF2's rule, without doing modulo
-}
gf2Multiply :: Natural -> Natural -> Natural
gf2Multiply x y =
  let setBitsInY = filter (testBit y) [0 .. naturalLog2 y]
  in  foldl' xor 0 . fmap (shiftL x) $ setBitsInY

gf2Modulo :: Natural -> Natural -> Natural
gf2Modulo x y =
  let step a = x `xor` (y `shiftL` (degree a - degree y))
      xs = iterate step x
  in  head $ dropWhile (\a -> degree a >= degree y) xs

{-|
-}
{-
  Implemented with extended euclidean algorithm.
  See https://en.wikipedia.org/wiki/Extended_Euclidean_algorithm
-}
gf2MulInv
  :: Natural -- ^reducing polynomial
  -> Natural -- ^value to invert
  -> Natural -- ^the inverse
gf2MulInv rp x = eesT . last $ takeWhile (\s -> eesR s /= 0) rs
  where
    rs = ExtEucState rp 1 0 : ExtEucState x 0 1 : zipWith next rs (tail rs)
    next :: ExtEucState -> ExtEucState -> ExtEucState
    next r1 r2 =
      let degDiff = degree (eesR r1) - degree (eesR r2)
          next' x1 x2 = gf2Modulo (x1 `xor` (x2 `shift` degDiff)) rp
      in  ExtEucState (next' (eesR r1) (eesR r2))
                      (next' (eesS r1) (eesS r2))
                      (next' (eesT r1) (eesT r2))

data ExtEucState = ExtEucState
  { eesR :: Natural
  , eesS :: Natural
  , eesT :: Natural
  }

degree :: Natural -> Int
degree 0 = 0
degree x = naturalLog2 x
