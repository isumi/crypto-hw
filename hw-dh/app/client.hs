module Main where

import           Data.String              (fromString)
import           Isumi.CryptoHW.DH.Client
import           System.Environment       (getArgs)

main :: IO ()
main = do
  (cmd:args) <- getArgs
  case cmd of
    "server" -> do
      let port = read (head args)
      startServer port
    "client" -> do
      let [host, port] = args
      startClient (fromString host) (read port)
    otherwise -> putStrLn "Invalid command"
