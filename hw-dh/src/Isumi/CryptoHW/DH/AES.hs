{-# LANGUAGE BangPatterns      #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PackageImports    #-}
{-# LANGUAGE RecordWildCards   #-}
{-# LANGUAGE TemplateHaskell   #-}

module Isumi.CryptoHW.DH.AES
  ( encryptMessage
  , decryptMessage
  , EncryptedMessage
  ) where

import           Crypto.Cipher.AES128
import           "cryptonite" Crypto.Random (MonadRandom, getRandomBytes)
import           Crypto.Types               (IV (..))
import           Data.ByteString            (ByteString)
import qualified Data.ByteString            as BS
import           Data.Maybe                 (fromJust)
import           Data.Store                 (Store)
import qualified Data.Store                 as Store
import           Data.Store.TH              (makeStore)
import           Data.Word                  (Word32)

encryptCBC :: ByteString -- ^key (16 byte long)
           -> ByteString -- ^iv (16 byte long)
           -> ByteString -- ^plain text
           -> ByteString -- ^cipher text
encryptCBC key plainText = undefined
  where
    key' :: AESKey128
    key' = fromJust (buildKey key)

data EncryptedMessage = EncryptedMessage
  { encMsgLen    :: !Word32 -- ^length of the plain text
  , encMsgIv     :: !ByteString -- ^initial vector
  , encMsgCipher :: !ByteString -- ^cipher text
  }

makeStore ''EncryptedMessage

encryptMessage :: MonadRandom m
               => ByteString -- ^key
               -> ByteString -- ^message
               -> m EncryptedMessage
encryptMessage key message = do
  iv <- getRandomBytes 16
  let !msgLen = BS.length message
      !newLen = wrapUp msgLen 16
      !paddedMsg = message <> (BS.replicate (newLen - msgLen) 0)
      !key' = fromJust (buildKey key) :: AESKey128
      !cipherText = fst (cbc key' (IV iv) paddedMsg)
  pure $ EncryptedMessage (fromIntegral msgLen) iv cipherText

wrapUp :: Int -- x
       -> Int -- n
       -> Int -- minimum y such that y >= x && y `mod` n == 0
wrapUp x n =
  if x `mod` n == 0
  then x
  else (x `div` n + 1) * n

decryptMessage :: ByteString -- ^key
               -> EncryptedMessage
               -> ByteString -- ^message
decryptMessage key EncryptedMessage{..} =
  let key' = fromJust $ buildKey key :: AESKey128
      plainText' =
        fst $ unCbc key' (IV encMsgIv) encMsgCipher
   in BS.take (fromIntegral encMsgLen) plainText'

key :: AESKey128
key = fromJust (buildKey "1234567890123456")

iv :: IV AESKey128
iv = IV "0000000000000000"
