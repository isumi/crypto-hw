{-# LANGUAGE TemplateHaskell #-}

module Isumi.CryptoHW.DH.KeyExchange
  ( keySize
  , Request(..)
  , mkRequest
  , mkReply
  , deriveKey
  , MonadRandom
  ) where

import           Crypto.Number.Generate (generateMax)
import           Crypto.Number.Prime    (generatePrime)
import           Crypto.Random.Types    (MonadRandom)
import           Data.Store             (Store)
import qualified Data.Store             as Store
import           Data.Store.TH          (makeStore)
import           GHC.Natural            (naturalFromInteger, naturalToInteger,
                                         powModNatural)
import           Numeric.Natural        (Natural)

data Request = Request
  { requestYa :: Natural
  , requestG  :: Natural
  , requestP  :: Natural
  } deriving (Show, Eq)

makeStore ''Request

instance Store Natural where
  size = case (Store.size :: Store.Size Integer) of
           Store.VarSize f -> Store.VarSize (f . naturalToInteger)
  poke = Store.poke . naturalToInteger
  peek = naturalFromInteger <$> Store.peek

-- |Bitsize of key
keySize :: Int
keySize = 128

mkRequest :: MonadRandom m
          => m (Request, Natural) -- ^request and xa
mkRequest = do
  p <- generatePrimeNat keySize
  xa <- generateMaxNat (2 ^ keySize)
  let g = 2
      ya = powModNatural g xa p
  pure (Request ya g p, xa)

mkReply :: MonadRandom m
        => Request
        -> m (Natural, Natural) -- ^yb and xb
mkReply (Request ya g p) = do
  xb <- generateMaxNat (2 ^ keySize)
  let yb = powModNatural g xb p
  pure (yb, xb)

deriveKey :: Natural -- ^p
          -> Natural -- ^my private data (xa or xb)
          -> Natural -- ^other's public data (yb or ya)
          -> Natural -- ^the shared key
deriveKey p xa yb = powModNatural yb xa p

generateMaxNat :: MonadRandom m => Natural -> m Natural
generateMaxNat range =
  naturalFromInteger <$> generateMax (fromIntegral range)

generatePrimeNat :: MonadRandom m => Int -> m Natural
generatePrimeNat size = naturalFromInteger <$> generatePrime size
