{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Isumi.CryptoHW.DH.Client
  ( startClient
  , startServer
  ) where

import           Conduit
import           Control.Concurrent            (forkIO)
import           Control.Concurrent.MVar
import           Data.ByteString               (ByteString)
import qualified Data.ByteString.Char8         as BS8
import qualified Data.Conduit.Network          as CondNet
import           Data.Foldable                 (traverse_)
import           Data.Maybe                    (maybe)
import qualified Data.Store                    as Store
import           Data.Text                     (Text)
import qualified Data.Text                     as Text
import           Data.Text.Encoding            (decodeUtf8, encodeUtf8)
import qualified Data.Text.IO                  as Text
import           Numeric.Natural               (Natural)

import           Isumi.CryptoHW.DH.AES
import qualified Isumi.CryptoHW.DH.KeyExchange as KeyExchange

startServer :: Int -- ^port to listen
            -> IO ()
startServer port = do
  Text.putStrLn $ "Listening on " <> showT port
  appDataM :: MVar CondNet.AppData <- newEmptyMVar
  keyM :: MVar ByteString <- newEmptyMVar
  forkIO $
    CondNet.runTCPServer (CondNet.serverSettings port "*") $ \appData -> do
      requestM <- runConduit $ CondNet.appSource appData
                            .| mapC Store.decodeEx
                            .| headC
      case requestM of
        Nothing -> putStrLn "No request"
        Just request -> do
          (yb, xb) <- KeyExchange.mkReply request
          runConduit $ yield (Store.encode yb)
                    .| CondNet.appSink appData
          let key :: ByteString = Store.encode $
                KeyExchange.deriveKey
                  (KeyExchange.requestP request)
                  xb
                  (KeyExchange.requestYa request)
          putMVar appDataM appData
          putMVar keyM key
          receiveMessages appData key
  appData <- takeMVar appDataM
  key <- takeMVar keyM
  stdinToConn appData key

startClient :: ByteString -- ^host
            -> Int -- ^port
            -> IO ()
startClient host port = do
  CondNet.runTCPClient (CondNet.clientSettings port host) onConnect
  where
    onConnect :: CondNet.AppData -> IO ()
    onConnect appData = do
      (request, xa) <- KeyExchange.mkRequest
      runConduit $ yield (Store.encode request)
                .| CondNet.appSink appData
      replyM <- runConduit $ CondNet.appSource appData
                          .| mapC Store.decodeEx
                          .| headC
      case replyM of
        Nothing -> putStrLn "No reply"
        Just reply -> do
          let key :: ByteString = Store.encode $
                KeyExchange.deriveKey (KeyExchange.requestP request) xa reply
          forkIO $ receiveMessages appData key
          stdinToConn appData key

receiveMessages :: CondNet.AppData -- ^connection
                -> ByteString -- ^key
                -> IO ()
receiveMessages appData key =
  let loop = do
        msgM <- runConduit $ CondNet.appSource appData
                          .| mapC decodeMessage
                          .| headC
        maybe (pure ()) (\msg -> Text.putStr msg >> loop) msgM
   in loop
  where
    decodeMessage = decodeUtf8 . decryptMessage key . Store.decodeEx

showT :: Show a => a -> Text
showT x = Text.pack (show x)

stdinToConn :: CondNet.AppData
            -> ByteString -- ^key
            -> IO ()
stdinToConn appData key =
  let loop = do
        line <- getLine
        msg <- encodeMessage line
        runConduit $ yield msg
                  .| CondNet.appSink appData
        loop
   in loop
  where
    encodeMessage line =
      fmap Store.encode . encryptMessage key
      . encodeUtf8 . Text.pack $ line ++ "\n"
