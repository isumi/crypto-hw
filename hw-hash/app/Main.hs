import qualified Data.ByteString            as BS
import           Data.ByteString.Char8      (pack)
import qualified Data.ByteString.Lazy       as LBS

import           Zelinf.CryptoHW.Hash.SHA1
import           Zelinf.CryptoHW.Hash.Utils (HexBS (..))

main :: IO ()
main = do
  putStrLn "Please input a string to hash"
  input <- getLine
  let theHash = sha1 $ LBS.fromStrict (pack input)
  putStrLn "Result is:"
  print $ HexBS theHash
