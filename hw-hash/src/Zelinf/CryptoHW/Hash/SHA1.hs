module Zelinf.CryptoHW.Hash.SHA1
  ( sha1
  ) where

import           Zelinf.CryptoHW.Hash.SHA1.Internal (sha1)
