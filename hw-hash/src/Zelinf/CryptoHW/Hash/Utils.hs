module Zelinf.CryptoHW.Hash.Utils
  ( HexBS(..)
  ) where

import           Numeric         (showHex)

import qualified Data.ByteString as BS

newtype HexBS = HexBS
  { unHexBS :: BS.ByteString
  } deriving Eq

instance Show HexBS where
  show (HexBS bs) = concat . fmap (\s -> if length s == 1 then '0' : s else s)
    . fmap (\s -> s []) . fmap showHex . BS.unpack $ bs
