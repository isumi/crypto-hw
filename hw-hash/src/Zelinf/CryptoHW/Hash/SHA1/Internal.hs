{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE RankNTypes                #-}
{-# LANGUAGE ScopedTypeVariables       #-}

module Zelinf.CryptoHW.Hash.SHA1.Internal
  (
  -- * SHA1 functions
    sha1
  , sha1Pad
  , kCon
  , fCompress
  , accGroup
  , generateExtendedWords
  , updateSHAState
  -- * Generic Utilities
  , bsGroupN
  , groupN
  , word8ToWord32
  , word64ToBS
  ) where

import           Control.Exception           (assert)
import           Control.Monad.ST            (ST, runST)
import           Control.Monad.State.Strict
import           Data.Bits
import qualified Data.ByteString             as BS
import           Data.ByteString.Builder     (toLazyByteString, word32BE,
                                              word32LE, word64BE, word64LE)
import qualified Data.ByteString.Lazy        as LBS
import           Data.Foldable               (traverse_)
import           Data.Functor.Identity
import           Data.Int                    (Int64)
import           Data.Monoid                 ((<>))
import           Data.STRef
import qualified Data.Vector                 as V
import           Data.Vector.Generic         (freeze, thaw)
import qualified Data.Vector.Unboxed         as UV
import qualified Data.Vector.Unboxed.Mutable as MUV
import           Data.Word                   (Word32, Word64, Word8)

sha1 :: LBS.ByteString -> BS.ByteString
sha1 input =
  let inputGroups = bsGroupN 64 $ sha1Pad input
      (resultVec :: UV.Vector Word32) = flip execState hVec $ traverse_ accGroup inputGroups
   in LBS.toStrict . mconcat . fmap (toLazyByteString . word32BE) . UV.toList $ resultVec

sha1Pad :: LBS.ByteString -> LBS.ByteString
sha1Pad x =
  let (l :: Word64) = fromIntegral $ LBS.length x
      (d :: Int64) = fromIntegral $ (64 - ((l + 9) `mod` 64)) `mod` 64
      padding = (0x80 `LBS.cons'` LBS.replicate d 0)
        <> word64ToBS (l * 8)
   in x <> padding

kCon :: Int -> Word32
kCon n = magicKVec UV.! (n `div` 20)

magicKVec :: UV.Vector Word32
magicKVec = UV.fromList
  [ 0x5A827999
  , 0x6ED9EBA1
  , 0x8F1BBCDC
  , 0xCA62C1D6
  ]

fCompress :: Int -> Word32 -> Word32 -> Word32 -> Word32
fCompress n = fVec V.! (n `div` 20)

fVec :: V.Vector (Word32 -> Word32 -> Word32 -> Word32)
fVec = V.fromList
  [ \b c d -> b .&. c .|. complement b .&. d
  , \b c d -> b `xor` c `xor` d
  , \b c d -> b .&.c .|. b .&. d .|. c .&. d
  , \b c d -> b `xor` c `xor` d
  ]

hVec :: UV.Vector Word32
hVec = UV.fromList
  [ 0x67452301
  , 0xEFCDAB89
  , 0x98BADCFE
  , 0x10325476
  , 0xC3D2E1F0
  ]

accGroup :: LBS.ByteString -> State (UV.Vector Word32) ()
accGroup group = do
  let (words :: [Word32]) = fmap (\[a, b, c, d] -> word8ToWord32 d c b a)
              . groupN 4 . LBS.unpack $ group
  -- There must be 16 words here
  assert (length words == 16) (pure ())
  let (extWords :: UV.Vector Word32) = generateExtendedWords (UV.fromList words)
  updateSHAState extWords

generateExtendedWords :: UV.Vector Word32 -- ^ 16 words
                      -> UV.Vector Word32 -- ^ 80 words
generateExtendedWords initial = runST $ do
  extWords <- thaw initial >>= flip MUV.grow 64
  traverse_ (\t -> do
    a <- MUV.read extWords (t - 3)
    b <- MUV.read extWords (t - 8)
    c <- MUV.read extWords (t - 14)
    d <- MUV.read extWords (t - 16)
    MUV.write extWords t (rotateL (a `xor` b `xor` c `xor` d) 1)
    ) [16..79]
  freeze extWords

updateSHAState :: UV.Vector Word32 -- ^extended words (80)
               -> State (UV.Vector Word32) ()
updateSHAState extWords = do
  prevS <- get
  let (newS :: UV.Vector Word32) = runST $ do
        a <- newSTRef (prevS UV.! 0)
        b <- newSTRef (prevS UV.! 1)
        c <- newSTRef (prevS UV.! 2)
        d <- newSTRef (prevS UV.! 3)
        e <- newSTRef (prevS UV.! 4)
        flip traverse_ [0..79] $ \t -> do
          [a', b', c', d', e'] :: [Word32] <- traverse readSTRef [a, b, c, d, e]
          let temp = rotateL a' 5 + fCompress t b' c' d' + e'
                + extWords UV.! t + kCon t
          readSTRef d >>= writeSTRef e
          readSTRef c >>= writeSTRef d
          (flip rotateL 30 <$> readSTRef b) >>= writeSTRef c
          readSTRef a >>= writeSTRef b
          writeSTRef a temp
        a' <- readSTRef a
        b' <- readSTRef b
        c' <- readSTRef c
        d' <- readSTRef d
        e' <- readSTRef e
        newS' <- MUV.new 5
        MUV.write newS' 0 $ (prevS UV.! 0) + a'
        MUV.write newS' 1 $ (prevS UV.! 1) + b'
        MUV.write newS' 2 $ (prevS UV.! 2) + c'
        MUV.write newS' 3 $ (prevS UV.! 3) + d'
        MUV.write newS' 4 $ (prevS UV.! 4) + e'
        freeze newS'
  put newS

bsGroupN :: Int64 -> LBS.ByteString -> [LBS.ByteString]
bsGroupN n bs
  | n == 0 = repeat LBS.empty
  | LBS.null bs = []
  | otherwise =
      let (block, remains) = LBS.splitAt n bs
       in block : bsGroupN n remains

groupN :: Int -> [a] -> [[a]]
groupN n [] = if n < 0 then repeat [] else []
groupN n xs =
  let (y, ys') = splitAt n xs
   in y : groupN n ys'

word8ToWord32 :: Word8 -> Word8 -> Word8 -> Word8 -> Word32
word8ToWord32 a b c d =
  let a' = fromIntegral a
      b' = fromIntegral b
      c' = fromIntegral c
      d' = fromIntegral d
   in a' .|. shiftL b' 8 .|. shiftL c' 16 .|. shiftL d' 24

word64ToBS :: Word64 -> LBS.ByteString
word64ToBS = toLazyByteString . word64BE
