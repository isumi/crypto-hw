{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Zelinf.CryptoHW.Hash.SHA1Spec
  ( spec
  ) where

import           Crypto.Hash                          (Digest, SHA1, hash)
import           Data.Byteable                        (toBytes)
import qualified Data.ByteString                      as BS
import qualified Data.ByteString.Lazy                 as LBS
import           Data.String
import           Numeric
import           Test.Hspec
import           Test.QuickCheck
import           Test.QuickCheck.Instances.ByteString
import           Zelinf.CryptoHW.Hash.SHA1
import           Zelinf.CryptoHW.Hash.Utils

spec :: Spec
spec = do
  describe "sha1" $ do
    it "Gives right answer for some sample data" $ do
      HexBS (sha1 "1") `shouldBe` HexBS (fromBytesLiteral "356A192B7913B04C54574D18C28D46E6395428AB")
    it "Gives right answer for arbitrary input" $ property $
      \(xs :: BS.ByteString) -> sha1 (LBS.fromStrict xs) `shouldBe` (toBytes (hash xs :: Digest SHA1))


fromBytesLiteral :: String -> BS.ByteString
fromBytesLiteral xs = BS.pack . fmap (fst . head . readHex) . groupN 2 $ xs

groupN :: Int -> [a] -> [[a]]
groupN n [] = if n < 0 then repeat [] else []
groupN n xs =
  let (y, ys') = splitAt n xs
   in y : groupN n ys'
