{-# LANGUAGE ScopedTypeVariables #-}

module Zelinf.CryptoHW.Hash.SHA1InternalSpec
  ( spec
  ) where

import qualified Data.ByteString.Lazy                 as LBS
import           Test.Hspec
import           Test.QuickCheck
import           Test.QuickCheck.Instances.ByteString

import           Zelinf.CryptoHW.Hash.SHA1.Internal   (bsGroupN, groupN,
                                                       word8ToWord32, sha1Pad)

spec :: Spec
spec = do
  describe "groupN" $ do
    it "splits into sublists(of specified size) that can be concat back" $ property $
      \(xs :: [Integer]) n -> n > 0 ==> let yss = groupN n xs
                in (concat yss == xs) && all (\ys -> length ys <= n) yss
    it "works for a particular sample" $
      groupN 3 "aaabbbcc" == ["aaa", "bbb", "cc"]
  describe "word8ToWord32" $
    it "combines Word8s in little endian" $
      word8ToWord32 0xAA 0xBB 0xCC 0xDD == 0xDDCCBBAA
  describe "bsGroupN" $ do
    it "splits into sub-bytestrings that can be concat back" $ property $
      \(xs :: LBS.ByteString) n -> n > 0 ==> let yss = bsGroupN n xs in mconcat yss == xs
  describe "sha1Pad" $ do
    it "pads any bytestring to a multiple of 64 bytes" $ property $
      \(xs :: LBS.ByteString) -> LBS.length (sha1Pad xs) `mod` 64 == 0
