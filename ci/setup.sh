#!/bin/bash

apt-get update && apt-get install -y libgmp-dev
mkdir bin

# download cabal
wget -O bin/cabal.tar.xz https://downloads.haskell.org/~cabal/cabal-install-latest/cabal-install-2.4.1.0-x86_64-unknown-linux.tar.xz
tar -xf bin/cabal.tar.xz cabal
mv cabal bin/
rm bin/cabal.tar.xz
GHC_INSTALL=`pwd`/ghc-install
export PATH=$GHC_INSTALL/bin:`pwd`/bin:$PATH

# download GHC
if ! type ghc-8.6.2; then
  wget -O ghc.tar.xz https://downloads.haskell.org/~ghc/8.6.2/ghc-8.6.2-x86_64-deb8-linux.tar.xz
  tar -xf ghc.tar.xz
  mkdir ghc-install
  cd ghc-8.6.2
  ./configure --prefix=$GHC_INSTALL
  make install
  cd ..
fi

cabal v2-update
