{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TupleSections       #-}

module Isumi.CryptoHW.DLP
  ( logPohligHellman
  , logShanks
  ) where

import           Control.Monad                              (join)
import           Crypto.Number.ModArithmetic                (inverse)
import           Data.Foldable                              (find)
import           Data.HashMap.Strict                        (HashMap)
import qualified Data.HashMap.Strict                        as HashMap
import           Data.Maybe                                 (catMaybes, isJust)
import           Data.MemoTrie                              (memo)
import           Data.Word                                  (Word64)
import           GHC.Exts                                   (fromList)
import           Math.NumberTheory.Moduli.Chinese           (chineseRemainder)
import           Math.NumberTheory.Moduli.Class             (Mod)
import           Math.NumberTheory.Moduli.DiscreteLogarithm (discreteLogarithm)
import           Math.NumberTheory.Powers                   (powMod)
import           Math.NumberTheory.Powers.Squares           (integerSquareRootRem)
import           Math.NumberTheory.Primes.Factorisation     (factorise)
import           Numeric.Natural                            (Natural)

logPohligHellman :: Natural -- ^ modulus (MUST be prime)
                 -> Natural -- ^ power
                 -> Natural -- ^ base
                 -> Maybe Natural -- ^ exponent
logPohligHellman p h g = natChineseRemainder congruence
  where
    n = p - 1
    ps = fmap (\(x, e) -> x ^ e `mod` p) $ natFactorise n
    gsOrHs x = fmap (\pi -> powMod x (n `div` pi) p) ps
    gs = gsOrHs g
    hs = gsOrHs h
    xs = zipWith3 (\h g p' -> log' p' p h g) hs gs ps :: [LogResult]
    congruence = filterMap (\(x, p) ->
      case x of
        AnyValue     -> Nothing
        SomeValue x' -> (, p) <$> x'
      ) $ zip xs ps

log' :: Natural -> Natural -> Natural -> Natural -> LogResult
log' n p h g =
    if g == 1 && h == 1
    then AnyValue
    else SomeValue $ logShanks n p h g

data LogResult = AnyValue | SomeValue (Maybe Natural)

isAnyValue :: LogResult -> Bool
isAnyValue AnyValue = True
isAnyValue _        = False

filterMap :: (a -> Maybe b) -> [a] -> [b]
filterMap f = catMaybes . fmap f

-- |
-- Compute discrete logarithm using baby-step giant-step method.
logShanks :: Natural -- ^ group size
          -> Natural -- ^ modulus
          -> Natural -- ^ power
          -> Natural -- ^ base
          -> Maybe Natural -- ^ exponent
logShanks n p power base = firstJust $ fmap
    (\(y, i) -> (\j -> (j * m + i) `mod` n) <$> HashMap.lookup y table)
    list
  where
    m = integerSquareCeil n
    table :: HashMap Natural Natural = fromList $
        fmap (\j -> (powMod base (m * j) p, j)) [0 .. m - 1]
    list = catMaybes $ fmap
        (\i -> tupFstSeqA
          ((\x -> (x * power) `mod` p) <$> natInverse (powMod base i p) p, i))
        [0 .. m - 1]

firstJust :: [Maybe a] -> Maybe a
firstJust = join . find isJust

tupFstSeqA :: (Maybe a, b) -> Maybe (a, b)
tupFstSeqA (Nothing, y) = Nothing
tupFstSeqA (Just x, y)  = Just (x, y)

int2Nat :: Integer -> Natural
int2Nat = fromIntegral

nat2Int :: Natural -> Integer
nat2Int = fromIntegral

integerSquareCeil :: Integral a => a -> a
integerSquareCeil x =
    let (y, r) = integerSquareRootRem x
     in if r == 0
        then y
        else y + 1

natInverse :: Natural -> Natural -> Maybe Natural
natInverse g m = fromIntegral <$> inverse (fromIntegral g) (fromIntegral m)

natFactorise :: Natural -> [(Natural, Int)]
natFactorise x = fmap (\(a, b) -> (int2Nat a, b)) $ memoFactorise (nat2Int x)

memoFactorise :: Integer -> [(Integer, Int)]
memoFactorise = memo factorise

natChineseRemainder :: [(Natural, Natural)] -> Maybe Natural
natChineseRemainder rms =
    int2Nat <$> chineseRemainder input
  where
    input = fmap (\(a, b) -> (nat2Int a, nat2Int b)) rms
