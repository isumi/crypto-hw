module Isumi.CryptoHW.RSA.Encrypt
  ( encrypt
  )
where

import           Control.Lens
import           Crypto.Number.ModArithmetic    ( expSafe )
import           Isumi.CryptoHW.RSA.KeyGen
import           Numeric.Natural

encrypt :: RSAKey -> Natural -> Natural
encrypt key x = fromIntegral (expSafe x' b n)
 where
  x' = fromIntegral x
  b  = fromIntegral $ key ^. rsaKey_exponent
  n  = fromIntegral $ key ^. rsaKey_divisor
