{-# LANGUAGE OverloadedStrings #-}

module Isumi.CryptoHW.RSA.Demo
  ( main
  )
where

import           Data.Bits
import           Data.ByteString                ( ByteString )
import qualified Data.ByteString               as BS
import qualified Data.ByteString.Char8         as CBS
import           Data.Foldable                  ( foldl' )
import           Data.Word                      ( Word8 )
import           Isumi.CryptoHW.RSA.Encrypt     ( encrypt )
import           Isumi.CryptoHW.RSA.KeyGen
import           Numeric                        ( showHex )
import           Numeric.Natural
import           System.Exit                    ( exitFailure )

main :: IO ()
main = do
  keyPairM <- generateKeyPairs 512 (byteStringToNaturalRaw "16337060")
  case keyPairM of
    Nothing -> do
      putStrLn "Can not generate public key"
      exitFailure
    Just (pvtKey, pubKey) -> do
      let plainText = byteStringToNaturalRaw "SUN YAT-SEN UNIVERSITY"
      putStrLn "Converted plain text: "
      putStrLn $ showHex plainText ""
      putStrLn ""

      let cipherText = encrypt pubKey plainText
      putStrLn "Cipher text is shown as below: "
      putStrLn $ showHex cipherText ""
      putStrLn ""
      putStrLn "Plain text (decrypted with private key): "
      putStrLn $ showHex (encrypt pvtKey cipherText) ""
      putStrLn ""

byteStringToNaturalRaw :: ByteString -> Natural
byteStringToNaturalRaw bs = foldl' xor 0 $ zipWith shiftL words [0, 8 ..]
  where words = fmap fromIntegral (BS.unpack bs) :: [Natural]
