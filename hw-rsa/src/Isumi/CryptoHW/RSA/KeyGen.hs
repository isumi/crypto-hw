{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell     #-}

module Isumi.CryptoHW.RSA.KeyGen
  ( RSAKey
  , rsaKey_length
  , rsaKey_exponent
  , rsaKey_divisor
  , generateKeyPairs
  )
where

import           Control.Lens
import           Control.Monad                  ( filterM )
import           Crypto.Number.ModArithmetic    ( inverse )
import           Crypto.Number.Prime            ( generatePrime )
import           Crypto.Random.Types            ( MonadRandom )
import           Data.Word                      ( Word32 )
import           Numeric.Natural

data RSAKey = RSAKey
  { _rsaKey_length   :: Word32
  , _rsaKey_divisor  :: Natural
  , _rsaKey_exponent :: Natural
  } deriving Show

makeLenses ''RSAKey

generateKeyPairs
  :: forall m
   . MonadRandom m
  => Word32 -- ^ key length
  -> Natural -- ^ exponent of private key
  -> m (Maybe (RSAKey, RSAKey)) -- ^ (private key, public key)
generateKeyPairs keyLen pvtExp = do
  let primes :: [m Natural] =
        repeat . fmap fromIntegral $ generatePrime (fromIntegral keyLen)
      primePairs = zipWith (\x y -> (,) <$> x <*> y) primes (tail primes)
  pqM <- findFirstWith (\(p, q) -> 1 == gcd pvtExp (phi p q)) primePairs
  case pqM of
    Nothing     -> pure Nothing
    Just (p, q) -> do
      let phiN = phi p q
          n    = p * q
      pure $ fmap (\pubKey -> (RSAKey keyLen n pvtExp, pubKey))
                  (RSAKey keyLen n <$> genericInverse pvtExp phiN)

phi :: Natural -> Natural -> Natural
phi p q = (p - 1) * (q - 1)

genericInverse
  :: (Integral i1, Integral i2, Integral i3) => i1 -> i2 -> Maybe i3
genericInverse x divisor =
  fromIntegral <$> inverse (fromIntegral x) (fromIntegral divisor)

findFirstWith :: Monad m => (a -> Bool) -> [m a] -> m (Maybe a)
findFirstWith _ []       = pure Nothing
findFirstWith p (x : xs) = do
  x' <- x
  if p x' then pure $ Just x' else findFirstWith p xs
